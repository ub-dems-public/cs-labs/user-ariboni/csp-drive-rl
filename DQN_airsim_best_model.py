import time
from AirsimEnv.DQN_classes import Agent, AirSimWrapper
import tensorflow as tf
from tensorflow.keras.layers import (Add, Conv2D, Dense, Flatten, Input,
                                     Lambda, Subtract)
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import VarianceScaling
from AirsimEnv.DQN_classes import (INPUT_SHAPE,NUM_ACTIONS)
import pandas as pd


#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
IP = "127.0.0.1"
PORT = 41451

TRAIN_STARTING_POINTS = [(88, -1, 0.2, 1, 0, 0, 0),
                         (127.5, 45, 0.2, 0.7, 0, 0, 0.7),
                         (30, 127.3, 0.2, 1, 0, 0, 0),
                         (-59.5, 126, 0.2, 0, 0, 0, 1),
                         (-127.2, 28, 0.2, 0.7, 0, 0, 0.7),
                         (-129, -48, 0.2, 0.7, 0, 0, -0.7),
                         (-90, -128.5, 0.2, 0, 0, 0, 1),
                         (0, -86, 0.2, 0.7, 0, 0, -0.7),
                         (62, -128.3, 0.2, 1, 0, 0, 0),
                         (127, -73, 0.2, 0.7, 0, 0, -0.7)]

TEST_STARTING_POINTS = [ (0.5,44,0.2,0.7,0,0,0.7),
                        (-75, -0.8, 0.2, 0, 0,0,1),
                        (-128.2, 45, 0.2, 0.7, 0, 0, 0.7),
                        (-0.5,-20, 0.2, 0.7, 0,0, -0.7),
                        (127, -38, 0.2, 0.7, 0, 0, 0.7),
                         (-6, 126.5,0,0,0,0,1),
                         (22, -127.5, 0.2, 1, 0, 0, 0),
                         (126.8,15,0.2,0.7,0,0,-0.7),
                         (-127.2,16,0.2,0.7,0,0,-0.7),
                         (-27,0,0.2,1,0,0,0)]

def build_deep_network(num_actions, input_shape, type_network):

    model_input = Input(shape=input_shape)
    x = Lambda(lambda img: img / 255)(model_input)  # normalize by 255
    x = Conv2D(32, (8, 8), strides=4, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_1")(x)
    x = Conv2D(64, (4, 4), strides=2, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_2")(x)
    x = Conv2D(64, (3, 3), strides=1, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_3")(x)
    x = Flatten()(x)

    if type_network == "D2QN":
        x = Dense(512, name="dense_drl_1")(x)
        output = Dense(num_actions, name="output_dense_actions")(x)
        model = Model(model_input, output)
    else:
        val_stream = Dense(512, name="dense_val_stream")(x)
        val = Dense(1, kernel_initializer=VarianceScaling(scale=2.), name="dense_val")(val_stream)
        adv_stream = Dense(512, name="dense_adv_action")(x)
        adv = Dense(num_actions, kernel_initializer=VarianceScaling(scale=2.), name="dense_adv")(adv_stream)

        # Combine streams into Q-Values
        reduce_mean = Lambda(lambda w: tf.reduce_mean(w, axis=1, keepdims=True))  # custom layer for reduce mean
        q_vals = Add()([val, Subtract()([adv, reduce_mean(adv)])])
        model = Model(model_input, q_vals)

    model.compile(Adam(), loss=tf.keras.losses.Huber())
    print(model.summary())

    return model

def evaluation_agent(path, type_network, num_evaluation, starting_points):

    airsim_wrapper = AirSimWrapper(ip=IP,port=PORT,input_shape=INPUT_SHAPE)
    MAIN_DQN = build_deep_network(num_actions=NUM_ACTIONS, input_shape=INPUT_SHAPE, type_network=type_network)
    MAIN_DQN.load_weights(path)
    TARGET_DQN = ""
    replay_memory = ""
    agent = Agent(MAIN_DQN, TARGET_DQN, replay_memory, num_actions=NUM_ACTIONS, input_shape=INPUT_SHAPE)

    df = pd.DataFrame(columns=["point", "reward", "time (s)", "frame"])

    for point in starting_points:
        print("Evaluation from: ", point)
        terminal = True
        for _ in range(num_evaluation):
            while True:
                if terminal:
                    start_time = time.time()
                    airsim_wrapper.reset(point)
                    episode_reward_sum = 0
                    frame_episode = 0
                    terminal = False

                # Step action
                action = agent.get_action(0, airsim_wrapper.state, eval=True)
                _, reward, terminal = airsim_wrapper.step(action)
                frame_episode += 1
                episode_reward_sum += reward

                if frame_episode == 2000:
                    terminal = True

                # On game-over
                if terminal:
                    df = df.append({"point": point, 'reward': episode_reward_sum, 'time (s)': time.time() - start_time, 'frame':frame_episode},
                                   ignore_index=True)
                    print({"point": point, 'reward': episode_reward_sum, 'time (s)': time.time() - start_time, 'frame':frame_episode})
                    break
    return df


if __name__ == "__main__":
    # simulatore 500x300

    model = "D3QN_TL_FINAL"
    path = "D3QN_FINAL_main_dqn.h5"
    type_network = "D3QN"

    df_train = evaluation_agent(path, type_network=type_network, num_evaluation=2, starting_points=TRAIN_STARTING_POINTS)
    df_test = evaluation_agent(path, type_network=type_network, num_evaluation=2, starting_points=TEST_STARTING_POINTS)





