import numpy as np
import random
import os
import time
import tensorflow as tf
import json
import cv2
from AirsimEnv.AirsimEnv import AirsimEnv

TOTAL_FRAMES = 1000000           # Total number of frames to train for
EPSILON_ANNELING_FRAMES = 500000
MAX_EPISODE_LENGTH = 2000        # Maximum length of an episode (in frames).  18000 frames / 60 fps = 5 minutes
FRAMES_BETWEEN_EVAL = 50000      # Number of frames between evaluations
UPDATE_FREQ = 10000               # Number of actions chosen between updating the target network

DISCOUNT_FACTOR = 0.99            # Gamma, how much to discount future rewards
MIN_REPLAY_MEMORY_SIZE = 100000     # The minimum size the replay buffer must be before we start to update the agent
MEM_SIZE = 300000                # The maximum size of the replay buffer

INPUT_SHAPE = (66,200,3)         # Size of the preprocessed input frame. With the current model architecture, anything below ~80 won't work.
BATCH_SIZE = 64                   # Number of samples the agent learns from at once
NUM_ACTIONS = 5

STARTING_POINTS = [(88,-1,0.2,1,0,0,0),
                        (127.5,45,0.2,0.7,0,0,0.7),
                        (30,127.3,0.2,1,0,0,0),
                        (-59.5, 126,0.2,0,0,0,1),
                        (-127.2,28,0.2,0.7,0,0,0.7),
                        (-129, -48,0.2, 0.7, 0, 0, -0.7),
                        (-90, -128.5, 0.2, 0, 0,0,1),
                        (0,-86, 0.2, 0.7, 0,0, -0.7),
                        (62, -128.3, 0.2, 1, 0, 0, 0),
                        (127, -73, 0.2, 0.7, 0, 0, -0.7)]

LOAD_REPLAY_MEMORY = True
WRITE_TENSORBOARD = True
WEIGHT_PATH = 'mnith_weights.h5' # weights of the pre-trained network

class AirSimWrapper:

    def __init__(self, input_shape, ip, port):
        self.env = AirsimEnv(ip, port)
        self.input_shape = input_shape
        self.state = np.empty(input_shape)

    def frameProcessor(self, frame):
        # assert frame.dim == 3
        frame = frame[40:136, 0:255, 0:3]
        frame = cv2.resize(frame, (self.input_shape[1], self.input_shape[0]), interpolation=cv2.INTER_NEAREST)
        return frame

    def reset(self, starting_point):

        observation = self.env.reset(starting_point)
        time.sleep(0.2)
        self.env.step(0)
        speed = self.env.client.getCarState().speed
        while speed < 3.0:
            speed = self.env.client.getCarState().speed

        self.state = self.frameProcessor(observation)

    def step(self, action):

        new_frame, reward, done, info = self.env.step(action)
        processed_frame = self.frameProcessor(new_frame)

        self.state = processed_frame

        return processed_frame, reward, done

class ReplayMemory:
    def __init__(self, size, input_shape):

        self.size = size
        self.input_shape = input_shape
        self.count = 0
        self.pos = 0

        self.actions = np.empty(self.size, dtype=np.int32)
        self.rewards = np.empty(self.size, dtype=np.float32)
        self.frames = np.empty((self.size, self.input_shape[0], self.input_shape[1], self.input_shape[2]),
                               dtype=np.uint8)
        self.terminal_flags = np.zeros(self.size, dtype=np.float32)

    def add_experience(self, action, frame, reward, terminal):

        if frame.shape != self.input_shape:
            ValueError("Dimension of frame is wrong!")

        self.actions[self.pos] = action
        self.frames[self.pos] = frame
        self.rewards[self.pos] = reward
        self.terminal_flags[self.pos] = terminal

        self.count = max(self.count, self.pos + 1)
        self.pos = (self.pos + 1) % self.size

    def get_minibatch(self, batch_size=64):

        if self.count < batch_size:
            raise ValueError("Not enough memories to get a minibatch")

        indices = []
        for _ in range(batch_size):
            while True:
                index = random.randint(0, self.count - 1)

                if index + 1 >= self.size:
                    continue
                if self.terminal_flags[index]:
                    continue
                break
            indices.append(index)

        states = []
        new_states = []

        for idx in indices:
            states.append(self.frames[idx,])
            new_states.append(self.frames[idx + 1,])

        return np.asarray(states), self.actions[indices], self.rewards[indices], np.asarray(new_states), \
               self.terminal_flags[indices]

    def save(self, folder_name):
        """Save the replay buffer to a folder"""

        if not os.path.isdir(folder_name):
            os.mkdir(folder_name)

        np.save(folder_name + '/actions.npy', self.actions)
        np.save(folder_name + '/frames.npy', self.frames)
        np.save(folder_name + '/rewards.npy', self.rewards)
        np.save(folder_name + '/terminal_flags.npy', self.terminal_flags)

    def load(self, folder_name):
        """Loads the replay buffer from a folder"""
        self.actions = np.load(folder_name + '/actions.npy')
        self.frames = np.load(folder_name + '/frames.npy')
        self.rewards = np.load(folder_name + '/rewards.npy')
        self.terminal_flags = np.load(folder_name + '/terminal_flags.npy')


class Agent:
    def __init__(self, main_dqn, target_dqn, replay_memory, num_actions, input_shape,
                 batch_size=64, eps_initial=1, eps_final=0.1, eps_final_frame=0.01,
                 eps_evaluation=0.0, eps_annealing_frames=150000, replay_memory_start_size=30000, max_frames=300000):

        self.main_dqn = main_dqn
        self.target_dqn = target_dqn
        self.num_actions = num_actions
        self.replay_memory = replay_memory
        self.replay_memory_start_size = replay_memory_start_size
        self.input_shape = input_shape
        self.batch_size = batch_size
        self.max_frames = max_frames

        # Epsilon information
        self.eps_initial = eps_initial
        self.eps_final = eps_final
        self.eps_final_frame = eps_final_frame
        self.eps_evaluation = eps_evaluation
        self.eps_annealing_frames = eps_annealing_frames

        # Slopes and intercepts for exploration decrease
        # (Credit to Fabio M. Graetz for this and calculating epsilon based on frame number)
        self.slope = -(self.eps_initial - self.eps_final) / self.eps_annealing_frames
        self.intercept = self.eps_initial - self.slope * self.replay_memory_start_size
        self.slope_2 = -(self.eps_final - self.eps_final_frame) / (
                    self.max_frames - self.eps_annealing_frames - self.replay_memory_start_size)
        self.intercept_2 = self.eps_final_frame - self.slope_2 * self.max_frames

    def calc_epsilon(self, frame_number, eval=False):
        """
        Get the appropriate epsilon value from a given frame number
        """
        if eval:
            return self.eps_evaluation
        elif frame_number < self.replay_memory_start_size:
            return self.eps_initial
        elif frame_number >= self.replay_memory_start_size and frame_number < self.replay_memory_start_size + self.eps_annealing_frames:
            return self.slope * frame_number + self.intercept
        elif frame_number >= self.replay_memory_start_size + self.eps_annealing_frames:
            return self.slope_2 * frame_number + self.intercept_2

    def get_action(self, frame_number, state, eval=False):
        """
        Query the DQN for an action given a state
        """
        eps = self.calc_epsilon(frame_number, eval)

        if frame_number % 100000 == 0:
            #print("frame number: ", frame_number)
            #print("epsilon value: ", eps)
            pass

        # with chance epsilon, take a random choice
        if np.random.rand(1) < eps:
            # st_time = time.time()
            action = np.random.randint(0, self.num_actions)
            time.sleep(1 / 25)
            return action

        # query the DQN for an action
        q_values = self.main_dqn.predict(state.reshape((-1, self.input_shape[0], self.input_shape[1], self.input_shape[2])))[0]
        action = q_values.argmax()
        return action

    def update_target_network(self):
        self.target_dqn.set_weights(self.main_dqn.get_weights())

    def add_experience(self, action, frame, reward, terminal):
        self.replay_memory.add_experience(action, frame, reward, terminal)

    def learn(self, batch_size, gamma, frame_number):
        """
        Sample a batch_size and use it to improve the DQN.
        Returns the loss between the predicted and target Q as a float
        """
        states, actions, rewards, new_states, terminal_flags = self.replay_memory.get_minibatch(batch_size)

        # main DQN estimates the best action in new states
        arg_q_max = self.main_dqn.predict(new_states).argmax(axis=1)

        # target DQN estimates the q values for new states
        future_q_values = self.target_dqn.predict(new_states)
        double_q = future_q_values[range(batch_size), arg_q_max]

        # calculate targets with Bellman equation
        # if terminal_flags == 1 (the state is terminal), target_q is equal to rewards
        target_q = rewards + (gamma * double_q * (1 - terminal_flags))

        # use targets to calculate loss and use loss to calculate gradients
        with tf.GradientTape() as tape:
            q_values = self.main_dqn(states)

            one_hot_actions = tf.keras.utils.to_categorical(actions, self.num_actions, dtype=np.float32)
            Q = tf.reduce_sum(tf.multiply(q_values, one_hot_actions), axis=1)

            error = Q - target_q
            loss = tf.keras.losses.Huber()(target_q, Q)

        model_gradients = tape.gradient(loss, self.main_dqn.trainable_variables)
        self.main_dqn.optimizer.apply_gradients(zip(model_gradients, self.main_dqn.trainable_variables))

        return float(loss.numpy()), error

    def save(self, folder_name, **kwargs):
        """Saves the Agent and all corresponding properties into a folder
        """

        # Create the folder for saving the agent
        if not os.path.isdir(folder_name):
            os.makedirs(folder_name)

        # Save DQN and target DQN
        self.main_dqn.save(folder_name + '/main_dqn.h5')
        self.target_dqn.save(folder_name + '/target_dqn.h5')

        # Save replay buffer
        self.replay_memory.save(folder_name + '/replay-memory')

        # Save meta
        with open(folder_name + '/meta.json', 'w+') as f:
            f.write(json.dumps({**{'memory_count': self.replay_memory.count, 'memory_pos': self.replay_memory.pos},
                                **kwargs}))  # save replay_memory information and any other information

    def load(self, folder_name, load_replay_memory=True):
        """Load a previously saved Agent from a folder
        """

        if not os.path.isdir(folder_name):
            raise ValueError(f'{folder_name} is not a valid directory')

        # Load DQNs
        self.main_dqn = tf.keras.models.load_model(folder_name + '/main_dqn.h5')
        self.target_dqn = tf.keras.models.load_model(folder_name + '/target_dqn.h5')

        # Load replay buffer
        if load_replay_memory:
            self.replay_memory.load(folder_name + '/replay-memory')

        # Load meta
        with open(folder_name + '/meta.json', 'r') as f:
            meta = json.load(f)

        if load_replay_memory:
            self.replay_memory.count = meta['memory_count']
            self.replay_memory.pos = meta['memory_pos']

        del meta['memory_count'], meta['memory_pos']  # we don't want to return this information
        return meta

