from tensorflow.python.summary.summary_iterator import summary_iterator
import os
import pandas as pd
from tensorflow.python.framework import tensor_util
import matplotlib.pyplot as plt

path = "/home/uc33014/vol/user/uc33014/user-ariboni/DRL/"
load = False
def create_dataframes(path):
    rewards = pd.DataFrame(columns=["step", "value"])
    losses = pd.DataFrame(columns=["step", "value"])
    eval_scores = pd.DataFrame(columns=["step", "value"])

    if load == False:
        path_run = path + "/tensorboard/"

        for file in os.listdir(path_run):
            for event in summary_iterator(path_run + file):
                for value in event.summary.value:
                    t = tensor_util.MakeNdarray(value.tensor)

                    if value.tag == "Reward":
                        rewards = rewards.append({"step": event.step, "value": float(t)}, ignore_index=True)
                    elif value.tag == "Loss":
                        losses = losses.append({"step": event.step, "value": float(t)}, ignore_index=True)
                    elif value.tag == "Evaluation score":
                        eval_scores = eval_scores.append({"step": event.step, "value": float(t)}, ignore_index=True)

        rewards = rewards.sort_values("step")
        losses = losses.sort_values("step")
        eval_scores = eval_scores.sort_values("step")

    return rewards, losses, eval_scores

if __name__ == '__main__':
    rewards_1, losses_1, eval_scores_1 = create_dataframes(path + "D2QN/")
    rewards_2, losses_2, eval_scores_2 = create_dataframes(path + "D3QN/")
    rewards_3, losses_3, eval_scores_3 = create_dataframes(path + "D2QN_TL/")
    rewards_4, losses_4, eval_scores_4 = create_dataframes(path + "D3QN_TL/")

    rewards_1["D2QN"] = rewards_1["value"]
    rewards_2["D3QN"] = rewards_2["value"]
    rewards_3["D2QN_TL"] = rewards_3["value"]
    rewards_4["D3QN_TL"] = rewards_4["value"]

    #plt.figure()
    ax = plt.gca()

    rewards_1.plot(x = 'step', y = 'D2QN', ax=ax)
    rewards_2.plot(x='step', y='D3QN', color="orange", ax=ax)
    rewards_3.plot(x='step', y='D2QN_TL', color="red", ax=ax)
    rewards_4.plot(x='step', y='D3QN_TL', color="black", ax=ax)
    plt.ylabel("avg reward")
    plt.xlabel("frames")
    plt.show()



