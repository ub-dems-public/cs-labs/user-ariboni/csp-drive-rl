import numpy as np
import os
import time
import random
import tensorflow as tf
from tensorflow.keras.layers import (Add, Conv2D, Dense, Flatten, Input,
                                     Lambda, Subtract)
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import VarianceScaling
from AirsimEnv.DQN_classes import ReplayMemory, Agent, AirSimWrapper
from AirsimEnv.DQN_classes import (BATCH_SIZE, DISCOUNT_FACTOR, FRAMES_BETWEEN_EVAL, INPUT_SHAPE,
                           LOAD_REPLAY_MEMORY, EPSILON_ANNELING_FRAMES, MEM_SIZE, NUM_ACTIONS,
                           MIN_REPLAY_MEMORY_SIZE, MAX_EPISODE_LENGTH,
                           TOTAL_FRAMES, UPDATE_FREQ, WRITE_TENSORBOARD, STARTING_POINTS, WEIGHT_PATH)

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

IP = "127.0.0.1"
PORT = 41451
TYPE_NETWORK = "D2QN"
TL = True

LOAD_FROM = None
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
random.seed(123)
np.random.seed(123)

if TL:
    SAVE_PATH = "/home/uc33014/vol/user/uc33014/user-ariboni/DRL/" + TYPE_NETWORK + "_TL/"
    TENSORBOARD_DIR = SAVE_PATH + "tensorboard/"
else:
    SAVE_PATH = "/home/uc33014/vol/user/uc33014/user-ariboni/DRL/" + TYPE_NETWORK + "/"
    TENSORBOARD_DIR = SAVE_PATH + "tensorboard/"


def build_deep_network(num_actions, input_shape, type_network, transfer_learning=TL):

    model_input = Input(shape=input_shape)
    x = Lambda(lambda img: img / 255)(model_input)  # normalize by 255
    x = Conv2D(32, (8, 8), strides=4, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_1")(x)
    x = Conv2D(64, (4, 4), strides=2, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_2")(x)
    x = Conv2D(64, (3, 3), strides=1, kernel_initializer=VarianceScaling(scale=2.), activation='relu', use_bias=False, name="conv_3")(x)
    x = Flatten()(x)

    if type_network == "D2QN":
        x = Dense(512, name="dense_drl_1")(x)
        output = Dense(num_actions, name="output_dense_actions")(x)
        model = Model(model_input, output)
    else:
        val_stream = Dense(512, name="dense_val_stream")(x)
        val = Dense(1, kernel_initializer=VarianceScaling(scale=2.), name="dense_val")(val_stream)
        adv_stream = Dense(512, name="dense_adv_action")(x)
        adv = Dense(num_actions, kernel_initializer=VarianceScaling(scale=2.), name="dense_adv")(adv_stream)

        # Combine streams into Q-Values
        reduce_mean = Lambda(lambda w: tf.reduce_mean(w, axis=1, keepdims=True))  # custom layer for reduce mean
        q_vals = Add()([val, Subtract()([adv, reduce_mean(adv)])])
        model = Model(model_input, q_vals)

    model.compile(Adam(), loss=tf.keras.losses.Huber())
    print(model.summary())

    if transfer_learning:
        model.load_weights(WEIGHT_PATH, by_name=True)
        print("Loading weights")
    else:
        print("Not loading weights")

    return model

if __name__ == "__main__":
    print(TENSORBOARD_DIR)

    airsim_wrapper = AirSimWrapper(ip=IP, port=PORT, input_shape=INPUT_SHAPE)
    writer = tf.summary.create_file_writer(TENSORBOARD_DIR)

    MAIN_DQN = build_deep_network(num_actions=NUM_ACTIONS, input_shape=INPUT_SHAPE, type_network=TYPE_NETWORK)
    TARGET_DQN = build_deep_network(num_actions=NUM_ACTIONS, input_shape=INPUT_SHAPE, type_network=TYPE_NETWORK)

    replay_memory = ReplayMemory(size=MEM_SIZE, input_shape=INPUT_SHAPE)
    agent = Agent(MAIN_DQN, TARGET_DQN, replay_memory, num_actions=NUM_ACTIONS, input_shape=INPUT_SHAPE,
                  batch_size=BATCH_SIZE, eps_annealing_frames=EPSILON_ANNELING_FRAMES,
                  replay_memory_start_size=MIN_REPLAY_MEMORY_SIZE, max_frames=TOTAL_FRAMES)

    if LOAD_FROM is None:
        frame_number = 0
        rewards = []
        loss_list = []
    else:
        print('Loading from', LOAD_FROM)
        meta = agent.load(LOAD_FROM, LOAD_REPLAY_MEMORY)

        # Apply information loaded from meta
        frame_number = meta['frame_number']
        rewards = meta['rewards']
        loss_list = meta['loss_list']

    initial_start_time = time.time()
    try:
        with writer.as_default():
            while frame_number < TOTAL_FRAMES:

                # Training
                epoch_frame = 0
                start_time_progress = time.time()

                while epoch_frame < FRAMES_BETWEEN_EVAL:

                    airsim_wrapper.reset(random.choice(STARTING_POINTS))

                    episode_reward_sum = 0

                    for _ in range(MAX_EPISODE_LENGTH):

                        frame_time = time.time()
                        # get action
                        action = agent.get_action(frame_number, airsim_wrapper.state)

                        # take step
                        processed_frame, reward, terminal = airsim_wrapper.step(action)
                        frame_number += 1
                        epoch_frame += 1
                        episode_reward_sum += reward

                        # add experience to replay memory
                        agent.add_experience(action=action, frame=processed_frame, reward=reward,
                                             terminal=terminal)

                        # update agent
                        if frame_number % 4 == 0 and agent.replay_memory.count > MIN_REPLAY_MEMORY_SIZE:
                            loss, _ = agent.learn(batch_size=BATCH_SIZE, gamma=DISCOUNT_FACTOR,
                                                  frame_number=frame_number)
                            loss_list.append(loss)
                        elif frame_number % 4 == 0:
                            time.sleep(0.10)

                        # Update target network
                        if frame_number % UPDATE_FREQ == 0 and frame_number > MIN_REPLAY_MEMORY_SIZE:
                            agent.update_target_network()

                        # Break the loop when the game is over
                        if terminal:
                            terminal = False
                            break

                        #print("Time of frame evaluation:", time.time() - frame_time)

                    rewards.append(episode_reward_sum)

                    # Output the progress every 100 games
                    if len(rewards) % 100 == 0:
                        # Write to TensorBoard
                        if WRITE_TENSORBOARD:
                            tf.summary.scalar('Reward', np.mean(rewards[-100:]), frame_number)
                            tf.summary.scalar('Loss', np.mean(loss_list[-100:]), frame_number)
                            writer.flush()

                        hours = divmod(time.time() - initial_start_time, 3600)
                        minutes = divmod(hours[1], 60)
                        minutes_100 = divmod(time.time() - start_time_progress, 60)
                        print(f'Game number: {str(len(rewards)).zfill(6)}  Frame number: {str(frame_number).zfill(8)}  '
                              f'Average reward: {np.mean(rewards[-100:]):0.1f}  Time taken: {(minutes_100[0]):.1f}  '
                              f'Total time taken: {(int(hours[0]))}:{(int(minutes[0]))}:{(minutes[1]):0.1f}')
                        start_time_progress = time.time()

                    # Save model
                    if len(rewards) % 500 == 0 and SAVE_PATH is not None:
                        agent.save(f'{SAVE_PATH}/save-{str(frame_number).zfill(8)}', frame_number=frame_number,
                                   rewards=rewards, loss_list=loss_list)

                # Evaluation every `FRAMES_BETWEEN_EVAL` frames

                eval_rewards = []
                evaluate_frame_number = 0


                terminal = True
                for point in STARTING_POINTS:
                    while True:
                        if terminal:
                            airsim_wrapper.reset(point)
                            episode_reward_sum = 0
                            frame_episode = 0
                            terminal = False

                        # Step action
                        action = agent.get_action(frame_number, airsim_wrapper.state, eval=True)
                        _, reward, terminal = airsim_wrapper.step(action)
                        evaluate_frame_number += 1
                        frame_episode += 1
                        episode_reward_sum += reward

                        # On game-over
                        if terminal:
                            print("Reward per episode: ", episode_reward_sum)
                            eval_rewards.append(episode_reward_sum)
                            break

                if len(eval_rewards) > 0:
                    final_score = np.mean(eval_rewards)
                else:
                    # In case the game is longer than the number of frames allowed
                    final_score = episode_reward_sum
                    # Print score and write to tensorboard

                print('Evaluation score:', final_score)
                if WRITE_TENSORBOARD:
                    tf.summary.scalar('Evaluation score', final_score, frame_number)
                    writer.flush()

            agent.save(f'{SAVE_PATH}/save-{str(frame_number).zfill(8)}', frame_number=frame_number,
                       rewards=rewards, loss_list=loss_list)

    except KeyboardInterrupt:
        print('\nTraining exited early.')
        writer.close()

        if SAVE_PATH is None:
            try:
                SAVE_PATH = input(
                    'Would you like to save the trained model? If so, type in a save path, otherwise, interrupt with ctrl+c. ')
            except KeyboardInterrupt:
                print('\nExiting...')

        if SAVE_PATH is not None:
            print('Saving...')
            agent.save(f'{SAVE_PATH}/save-{str(frame_number).zfill(8)}', frame_number=frame_number, rewards=rewards,
                       loss_list=loss_list)
            print('Saved.')